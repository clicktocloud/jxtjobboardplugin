package com.ctc.jobboard.jxt.request;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import javax.xml.bind.JAXBException;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.ctc.jobboard.jxt.domain.ConsultantRequest;
import com.ctc.jobboard.marshaller.MarshallerHelper;
import com.ctc.jobboard.util.JobBoardHelper;

public class ConsultantRequestTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() throws JAXBException, IOException {
		String xml = JobBoardHelper.readFileToString("/ConsultantRequestSample.xml");
		ConsultantRequest r = MarshallerHelper.convertXMLtoObject(xml, ConsultantRequest.class);
		
		assertEquals("USERNAME", r.getUserName());
		assertEquals("PASSWORD", r.getPassword());
		assertEquals(111, r.getAdvertiserId().intValue());
		
		
		
		
		
	}

}
