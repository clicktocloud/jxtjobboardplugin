package com.ctc.jobboard.jxt.component;

import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.ctc.jobboard.core.JobBoard;
import com.ctc.jobboard.core.JobBoardRequestPackage;
import com.ctc.jobboard.core.JobBoardRequestPackage.MethodType;
import com.ctc.jobboard.core.JobBoardResponse;
import com.ctc.jobboard.core.JobContentXmlConverter;
import com.ctc.jobboard.exception.AccountException;
import com.ctc.jobboard.jxt.domain.APIReturn;
import com.ctc.jobboard.jxt.domain.ArchiveJobRequest;
import com.ctc.jobboard.jxt.domain.ArrayOfJob;
import com.ctc.jobboard.jxt.domain.BaseRequest;
import com.ctc.jobboard.jxt.domain.ConsultantRequest;
import com.ctc.jobboard.jxt.domain.DefaultRequest;
import com.ctc.jobboard.jxt.domain.Job;
import com.ctc.jobboard.jxt.domain.JobPostRequest;
import com.ctc.jobboard.jxt.domain.JobPostSummary;
import com.ctc.jobboard.jxt.domain.ext.JobPostResponse;
import com.ctc.jobboard.util.BasicConfig;

public abstract class JXTJobBoard extends JobBoard{
	
	public static final String DEFAULT_BOARD_NAME;
	public static final String DEFAULT_BOARD_API_NAME ;
	
	
	public static final String SERVICE_DOMAIN;
	public static final String ACCEPT_XML = "application/xml";
	public static final String CONTENT_TYPE_XML = "application/xml";
	
	
	static{
		SERVICE_DOMAIN = StringUtils.isEmpty( BasicConfig.get("jxt_endpoint")) ? "http://webservices.jxt.net.au" : BasicConfig.get("jxt_endpoint");
		DEFAULT_BOARD_NAME = StringUtils.isEmpty( BasicConfig.get("jxt_default_jobboard_name")) ? "JXT" : BasicConfig.get("jxt_default_jobboard_name");
		DEFAULT_BOARD_API_NAME = StringUtils.isEmpty( BasicConfig.get("jxt_default_jobboard_apiname")) ? "JXT__c" : BasicConfig.get("jxt_default_jobboard_apiname");
		
		requestEndpoints.put(JobPostRequest.class, SERVICE_DOMAIN +"/Post/Jobs");
		requestEndpoints.put(DefaultRequest.class, SERVICE_DOMAIN +"/Get/DefaultList");
		requestEndpoints.put(ConsultantRequest.class, SERVICE_DOMAIN +"/Get/Consultants");
		requestEndpoints.put(ArchiveJobRequest.class, SERVICE_DOMAIN +"/Archive/Jobs");
	}
	
	{
		successCodes.add(200);
		
	}
	
	public JXTJobBoard() {
		this(DEFAULT_BOARD_NAME);
		
		
	}

	public JXTJobBoard(String jobBoardName) {
		super(jobBoardName, new JobContentXmlConverter());
		
		
	}
	
	
	public JXTJobBoard(String jobBoardName, String jobBoardSfApiName) {
		super(jobBoardName, jobBoardSfApiName, new JobContentXmlConverter());
		
		
	}
	
	public JxtAccount buildAccount(String advertiserId, String username, String password){
		return new JxtAccount(username, password, advertiserId);
	}
	
	
	protected void setCredential(BaseRequest request){
		
		JxtAccount jxtAccount = (JxtAccount) getAccount();
		if(jxtAccount == null){
			throw new AccountException("Can not find available JXT Account !");
		}

		//set credentials
		request.setUserName(jxtAccount.getUsername());
		request.setPassword(jxtAccount.getPassword());
		request.setAdvertiserId( jxtAccount.getIntAdvertiserId());
		
	}
	
	
	protected JobPostResponse createJobPostResponse(APIReturn code, String message, String action, String actionMessage, List<String> referenceNos){
		if( referenceNos == null || referenceNos.size() == 0){
			return null;
		}
		
		JobPostResponse response = new JobPostResponse();
		response.setSummary(new JobPostSummary());
		response.getSummary().setSent(1);
		response.getSummary().setFailed(1);
		response.getSummary().setInserted(0);
		response.getSummary().setUpdated(0);
		response.getSummary().setArchived(0);
		
		
		response.setResponseCode(code);
		response.setResponseMessage(message);
		response.setJobPosting( new ArrayOfJob());
		
		for(String no : referenceNos){
			Job job = new Job();
			job.setAction(action);
			job.setMessage(actionMessage);
			job.setReferenceNo( no );
			response.getJobPosting().getJob().add(job);
		}

		return response;
	}
	

	protected JobPostResponse createJobPostResponse(APIReturn code, String message, String action, String actionMessage, String referenceNo){
		if( referenceNo == null ){
			return null;
		}
		
		return createJobPostResponse(code, message, action, actionMessage, Arrays.asList(new String[]{referenceNo}));
		
		
	}
	
	
	
	
	protected <R extends JobBoardResponse> R execute(BaseRequest request, Class<R> clazz) {
		JobBoardRequestPackage requestPackage = new JobBoardRequestPackage();
		requestPackage.setRequest(request);
		requestPackage.setMethodType(MethodType.POST);
		requestPackage.addHeader("Accept", ACCEPT_XML);
		requestPackage.setContentType(CONTENT_TYPE_XML);
		
		return super.execute(requestPackage, clazz);
		
		//return super.execute(request, clazz);
		
	}
	
	


}
