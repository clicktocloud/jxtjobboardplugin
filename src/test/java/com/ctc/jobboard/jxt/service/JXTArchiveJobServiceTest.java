package com.ctc.jobboard.jxt.service;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.ctc.jobboard.jxt.domain.ext.JobPostResponse;
import com.ctc.jobboard.jxt.utils.JXTConfig;

public class JXTArchiveJobServiceTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() throws IOException {
		JXTArchiveJobService archiveService = new JXTArchiveJobService("JXT","JXT__c");
	
		List<String> referenceNos = Arrays.asList(new String[]{"00D90000000gtFz:a0090000005cDR5AAM"});
		//JobPostResponse response = archiveService.archiveJob("00D90000000gtFz", referenceNos);
		JobPostResponse response = archiveService.archiveJob("00D90000000gtFz", referenceNos, JXTConfig.JXT_TEST_ADVERTISER_ID, JXTConfig.JXT_TEST_USERNAME, JXTConfig.JXT_TEST_PASSWORD, "");
		
		System.out.println("Archived : " + response.getSummary().getArchived());
		
	}

}
