package com.ctc.jobboard.jxt.component;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;

import com.ctc.jobboard.core.JobBoard;
import com.ctc.jobboard.core.JobBoardResponse;
import com.ctc.jobboard.core.JobBoardResponsePackage;
import com.ctc.jobboard.jxt.domain.APIReturn;
import com.ctc.jobboard.jxt.domain.BaseResponse;
import com.ctc.jobboard.jxt.domain.Job;
import com.ctc.jobboard.jxt.domain.ext.JobPostResponse;

public class JXTResponsePackage implements JobBoardResponsePackage {
	
	private BaseResponse response;
	private Set<Integer> successCodes;
	
	private Summary summary;
	
	private Boolean hasErrors;
	private String errorMessage;
	private List<String> errorMessages;
	private List<Error> errors ;
	private List<Error> jobErrors;
	
	private List<Advertisement> created;
	private List<Advertisement> updated;
	private List<Advertisement> archived;
	
	private List<Advertisement> insertUpdated;
	

	public JXTResponsePackage(BaseResponse response,Set<Integer> successCodes) {
		super();
		this.response = response;
		this.successCodes = successCodes;
	}
	
	@Override
	public Summary getSummary() {
		if( summary != null){
			return summary;
		}
		
		summary  = new Summary();
		if(response instanceof JobPostResponse){
			JobPostResponse response = (JobPostResponse) this.response;
			if(response.getSummary() != null){
				
				summary.setSent(response.getSummary().getSent());
				summary.setInserted(response.getSummary().getInserted());
				summary.setUpdated(response.getSummary().getUpdated());
				summary.setArchived(response.getSummary().getArchived());
				summary.setFailed(response.getSummary().getFailed());
				
			}
			
		}
		
		return summary;
		
	}


	@Override
	final public boolean hasErrors() {
		if( hasErrors != null)
			return hasErrors;
		
		if(response == null)
			hasErrors = true;
		else
			hasErrors = (response.getHttpStatusCode()!=null && !successCodes.contains(response.getHttpStatusCode())) || response.getResponseCode() != APIReturn.SUCCESS || hasJobErrors();
		return hasErrors;
	}
	
	final public String getErrorMessage(){
		if(errorMessage != null ){
			return errorMessage;
		}
		errorMessage = "";
		List<String> messages = getErrorMessages();
		if(messages != null && messages.size() > 0){
			errorMessage = messages.get( 0 );
		}
		
		return errorMessage;
	}
	
	final public List<String> getErrorMessages(){
		if( errorMessages != null){
			return errorMessages;
		}
		errorMessages = new ArrayList<String>();
		
		List<Error> errors = getErrors();
		for(Error error : errors){
			errorMessages.add(  error.getCode() + " - " + error.getMessage() );

		}
		
		return errorMessages;
	}


	@Override
	final public  List<Error> getErrors( ) {
		if( errors != null )
			return errors;
		
		errors =  new ArrayList<Error>();
		
		
		if( hasErrors() ){
			if(response == null){
				
				errors.add(new JobBoardResponsePackage.Error("Others","Unknown exception !"));
			}else {
	
				if(response.getHttpStatusCode() != null){
					errors.add(new JobBoardResponsePackage.Error(response.getHttpStatusCode()+"" , response.getHttpMessage()));
				}else if(response.getResponseCode() != APIReturn.SUCCESS){
					errors.add(new JobBoardResponsePackage.Error(response.getResponseCode().name(), response.getResponseMessage()));
				}
			}

		}
		
		return errors;
	}


	@Override
	final public List<Error> getJobErrors() {
		if( jobErrors != null)
			return jobErrors;
		
		jobErrors =  new ArrayList<Error>();
		
		if( hasJobErrors() ){
			JobPostResponse response = (JobPostResponse) this.response;
			List<Job> jobs = response.getErrors();
		
			for(Job job : jobs){
				
				String jobRefCode = job.getReferenceNo();
				
			
				if( ! StringUtils.isEmpty( jobRefCode )){
					
					jobErrors.add(new JobBoardResponsePackage.Error(jobRefCode, job.getMessage()));
				}
			}
		
		}
		
		
		return jobErrors;
	}
	
	


	@Override
	final public List<Advertisement> getArchived() {
		if( archived != null)
			return archived;
		archived = new ArrayList<Advertisement>();
		if(response != null && response instanceof JobPostResponse){
			JobPostResponse response = (JobPostResponse) this.response;
			archived = getAdvertisements( response.getArchives() );
	
		}
		
		return archived;
		
	}
	



	@Override
	final public List<Advertisement> getInsertUpdated() {
		if( insertUpdated != null)
			return insertUpdated;
		
		insertUpdated = new ArrayList<Advertisement>();
		if(response != null && response instanceof JobPostResponse){
			JobPostResponse response = (JobPostResponse) this.response;
			insertUpdated = getAdvertisements( response.getInsertUpdates());
	
		}
		
		return insertUpdated;
		
		
	}


	@Override
	final public List<Advertisement> getCreated() {
		if( created != null)
			return created;
		
		created = new ArrayList<Advertisement>();
		if(response != null && response instanceof JobPostResponse){
			JobPostResponse response = (JobPostResponse) this.response;
			created = getAdvertisements( response.getInserts());
	
		}
		
		return created;
	}


	@Override
	final public List<Advertisement> getUpdated() {
		if( updated != null )
			return updated;
		updated = new ArrayList<Advertisement>();
		if(response != null && response instanceof JobPostResponse){
			JobPostResponse response = (JobPostResponse) this.response;
			updated = getAdvertisements( response.getUpdates());
		}
		
		return updated;
	}
	
	private List<Advertisement> getAdvertisements(List<Job> jobs){
		List<Advertisement> ads = new ArrayList<Advertisement>();
		if(jobs != null  && jobs.size() >0){
			for(Job job : jobs){
				
				String aid = JobBoard.getAdId( job.getReferenceNo() );
				if( ! StringUtils.isEmpty( aid )){
					String onlineAdUrl = job.getURL();
					
					String onlineJobId = "";
					if(onlineAdUrl != null && onlineAdUrl.indexOf("/") > 0 && onlineAdUrl.indexOf("/") < onlineAdUrl.length() - 1){
						onlineJobId = onlineAdUrl.substring(onlineAdUrl.lastIndexOf("/") + 1);
					}
					String action = job.getAction();
					Advertisement a = new Advertisement( aid, onlineJobId, onlineAdUrl,action );
					a.setOnlineOtherUrl(onlineAdUrl);
					ads.add(a);
				}
			}
		}
		return ads;
	}
	
	private boolean hasJobErrors(){
		if(response != null && response instanceof JobPostResponse){
			JobPostResponse response = (JobPostResponse) this.response;
			List<Job> jobs = response.getErrors();
			return jobs != null && jobs.size() > 0;
		}
		
		return false;
	}

	@Override
	public JobBoardResponse getResponse() {
		return this.response;
	}


	
}
