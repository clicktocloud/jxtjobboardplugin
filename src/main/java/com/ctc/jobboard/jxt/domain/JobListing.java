//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2016.11.29 at 01:57:13 PM AEDT 
//


package com.ctc.jobboard.jxt.domain;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for JobListing complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="JobListing">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="JobAdType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ReferenceNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="JobTitle" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="JobUrl" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ShortDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Bulletpoints" type="{http://schemas.servicestack.net/types}BulletPoints" minOccurs="0"/>
 *         &lt;element name="JobFullDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ContactDetails" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CompanyName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ConsultantID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PublicTransport" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ResidentsOnly" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="IsQualificationsRecognised" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="ShowLocationDetails" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="JobTemplateID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AdvertiserJobTemplateLogoID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Categories" type="{http://schemas.servicestack.net/types}ArrayOfCategory" minOccurs="0"/>
 *         &lt;element name="ListingClassification" type="{http://schemas.servicestack.net/types}ListingClassification" minOccurs="0"/>
 *         &lt;element name="Salary" type="{http://schemas.servicestack.net/types}Salary" minOccurs="0"/>
 *         &lt;element name="ApplicationMethod" type="{http://schemas.servicestack.net/types}ApplicationMethod" minOccurs="0"/>
 *         &lt;element name="Referral" type="{http://schemas.servicestack.net/types}ReferralFee" minOccurs="0"/>
 *         &lt;element name="ExpiryDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "JobListing", propOrder = {
    "jobAdType",
    "referenceNo",
    "jobTitle",
    "jobUrl",
    "shortDescription",
    "bulletpoints",
    "jobFullDescription",
    "contactDetails",
    "companyName",
    "consultantID",
    "publicTransport",
    "residentsOnly",
    "isQualificationsRecognised",
    "showLocationDetails",
    "jobTemplateID",
    "advertiserJobTemplateLogoID",
    "categories",
    "listingClassification",
    "salary",
    "applicationMethod",
    "referral",
    "expiryDate"
})
@XmlRootElement(name="JobListing")
public class JobListing {

    @XmlElement(name = "JobAdType", nillable = true)
    protected String jobAdType;
    @XmlElement(name = "ReferenceNo", nillable = true)
    protected String referenceNo;
    @XmlElement(name = "JobTitle", nillable = true)
    protected String jobTitle;
    @XmlElement(name = "JobUrl", nillable = true)
    protected String jobUrl;
    @XmlElement(name = "ShortDescription", nillable = true)
    protected String shortDescription;
    @XmlElement(name = "Bulletpoints", nillable = true)
    protected BulletPoints bulletpoints;
    @XmlElement(name = "JobFullDescription", nillable = true)
    protected String jobFullDescription;
    @XmlElement(name = "ContactDetails", nillable = true)
    protected String contactDetails;
    @XmlElement(name = "CompanyName", nillable = true)
    protected String companyName;
    @XmlElement(name = "ConsultantID", nillable = true)
    protected String consultantID;
    @XmlElement(name = "PublicTransport", nillable = true)
    protected String publicTransport;
    @XmlElement(name = "ResidentsOnly", nillable = true)
    protected Boolean residentsOnly;
    @XmlElement(name = "IsQualificationsRecognised", nillable = true)
    protected Boolean isQualificationsRecognised;
    @XmlElement(name = "ShowLocationDetails", nillable = true)
    protected Boolean showLocationDetails;
    @XmlElement(name = "JobTemplateID", nillable = true)
    protected String jobTemplateID;
    @XmlElement(name = "AdvertiserJobTemplateLogoID", nillable = true)
    protected String advertiserJobTemplateLogoID;
    @XmlElement(name = "Categories", nillable = true)
    protected ArrayOfCategory categories;
    @XmlElement(name = "ListingClassification", nillable = true)
    protected ListingClassification listingClassification;
    @XmlElement(name = "Salary", nillable = true)
    protected Salary salary;
    @XmlElement(name = "ApplicationMethod", nillable = true)
    protected ApplicationMethod applicationMethod;
    @XmlElement(name = "Referral", nillable = true)
    protected ReferralFee referral;
    @XmlElement(name = "ExpiryDate", nillable = true)
    protected String expiryDate;

    /**
     * Gets the value of the jobAdType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getJobAdType() {
        return jobAdType;
    }

    /**
     * Sets the value of the jobAdType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setJobAdType(String value) {
        this.jobAdType = value;
    }

    /**
     * Gets the value of the referenceNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReferenceNo() {
        return referenceNo;
    }

    /**
     * Sets the value of the referenceNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReferenceNo(String value) {
        this.referenceNo = value;
    }

    /**
     * Gets the value of the jobTitle property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getJobTitle() {
        return jobTitle;
    }

    /**
     * Sets the value of the jobTitle property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setJobTitle(String value) {
        this.jobTitle = value;
    }

    /**
     * Gets the value of the jobUrl property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getJobUrl() {
        return jobUrl;
    }

    /**
     * Sets the value of the jobUrl property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setJobUrl(String value) {
        this.jobUrl = value;
    }

    /**
     * Gets the value of the shortDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShortDescription() {
        return shortDescription;
    }

    /**
     * Sets the value of the shortDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShortDescription(String value) {
        this.shortDescription = value;
    }

    /**
     * Gets the value of the bulletpoints property.
     * 
     * @return
     *     possible object is
     *     {@link BulletPoints }
     *     
     */
    public BulletPoints getBulletpoints() {
        return bulletpoints;
    }

    /**
     * Sets the value of the bulletpoints property.
     * 
     * @param value
     *     allowed object is
     *     {@link BulletPoints }
     *     
     */
    public void setBulletpoints(BulletPoints value) {
        this.bulletpoints = value;
    }

    /**
     * Gets the value of the jobFullDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getJobFullDescription() {
        return jobFullDescription;
    }

    /**
     * Sets the value of the jobFullDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setJobFullDescription(String value) {
        this.jobFullDescription = value;
    }

    /**
     * Gets the value of the contactDetails property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContactDetails() {
        return contactDetails;
    }

    /**
     * Sets the value of the contactDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContactDetails(String value) {
        this.contactDetails = value;
    }

    /**
     * Gets the value of the companyName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCompanyName() {
        return companyName;
    }

    /**
     * Sets the value of the companyName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCompanyName(String value) {
        this.companyName = value;
    }

    /**
     * Gets the value of the consultantID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConsultantID() {
        return consultantID;
    }

    /**
     * Sets the value of the consultantID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConsultantID(String value) {
        this.consultantID = value;
    }

    /**
     * Gets the value of the publicTransport property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPublicTransport() {
        return publicTransport;
    }

    /**
     * Sets the value of the publicTransport property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPublicTransport(String value) {
        this.publicTransport = value;
    }

    /**
     * Gets the value of the residentsOnly property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isResidentsOnly() {
        return residentsOnly;
    }

    /**
     * Sets the value of the residentsOnly property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setResidentsOnly(Boolean value) {
        this.residentsOnly = value;
    }

    /**
     * Gets the value of the isQualificationsRecognised property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsQualificationsRecognised() {
        return isQualificationsRecognised;
    }

    /**
     * Sets the value of the isQualificationsRecognised property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsQualificationsRecognised(Boolean value) {
        this.isQualificationsRecognised = value;
    }

    /**
     * Gets the value of the showLocationDetails property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isShowLocationDetails() {
        return showLocationDetails;
    }

    /**
     * Sets the value of the showLocationDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setShowLocationDetails(Boolean value) {
        this.showLocationDetails = value;
    }

    /**
     * Gets the value of the jobTemplateID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getJobTemplateID() {
        return jobTemplateID;
    }

    /**
     * Sets the value of the jobTemplateID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setJobTemplateID(String value) {
        this.jobTemplateID = value;
    }

    /**
     * Gets the value of the advertiserJobTemplateLogoID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAdvertiserJobTemplateLogoID() {
        return advertiserJobTemplateLogoID;
    }

    /**
     * Sets the value of the advertiserJobTemplateLogoID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAdvertiserJobTemplateLogoID(String value) {
        this.advertiserJobTemplateLogoID = value;
    }

    /**
     * Gets the value of the categories property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfCategory }
     *     
     */
    public ArrayOfCategory getCategories() {
        return categories;
    }

    /**
     * Sets the value of the categories property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfCategory }
     *     
     */
    public void setCategories(ArrayOfCategory value) {
        this.categories = value;
    }

    /**
     * Gets the value of the listingClassification property.
     * 
     * @return
     *     possible object is
     *     {@link ListingClassification }
     *     
     */
    public ListingClassification getListingClassification() {
        return listingClassification;
    }

    /**
     * Sets the value of the listingClassification property.
     * 
     * @param value
     *     allowed object is
     *     {@link ListingClassification }
     *     
     */
    public void setListingClassification(ListingClassification value) {
        this.listingClassification = value;
    }

    /**
     * Gets the value of the salary property.
     * 
     * @return
     *     possible object is
     *     {@link Salary }
     *     
     */
    public Salary getSalary() {
        return salary;
    }

    /**
     * Sets the value of the salary property.
     * 
     * @param value
     *     allowed object is
     *     {@link Salary }
     *     
     */
    public void setSalary(Salary value) {
        this.salary = value;
    }

    /**
     * Gets the value of the applicationMethod property.
     * 
     * @return
     *     possible object is
     *     {@link ApplicationMethod }
     *     
     */
    public ApplicationMethod getApplicationMethod() {
        return applicationMethod;
    }

    /**
     * Sets the value of the applicationMethod property.
     * 
     * @param value
     *     allowed object is
     *     {@link ApplicationMethod }
     *     
     */
    public void setApplicationMethod(ApplicationMethod value) {
        this.applicationMethod = value;
    }

    /**
     * Gets the value of the referral property.
     * 
     * @return
     *     possible object is
     *     {@link ReferralFee }
     *     
     */
    public ReferralFee getReferral() {
        return referral;
    }

    /**
     * Sets the value of the referral property.
     * 
     * @param value
     *     allowed object is
     *     {@link ReferralFee }
     *     
     */
    public void setReferral(ReferralFee value) {
        this.referral = value;
    }

    /**
     * Gets the value of the expiryDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExpiryDate() {
        return expiryDate;
    }

    /**
     * Sets the value of the expiryDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExpiryDate(String value) {
        this.expiryDate = value;
    }

}
