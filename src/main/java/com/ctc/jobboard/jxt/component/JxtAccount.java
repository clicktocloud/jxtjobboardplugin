package com.ctc.jobboard.jxt.component;

import com.ctc.jobboard.component.BasicAccount;

public class JxtAccount extends BasicAccount {
	
	
	
	
	
	public String getAdvertiserId() {
		if(super.getAdvertiserId()==null)
			super.setAdvertiserId("0");
			
		return super.getAdvertiserId();
	}
	
	public JxtAccount(String username, String password, String advertiserId) {
		super(username, password, advertiserId);
		
	}
	
	public Integer getIntAdvertiserId(){
		Integer id = 0;
		
		try {
			id = Integer.valueOf(getAdvertiserId());
		} catch (NumberFormatException e) {
			
		}
		
		return id;
	}
	
	public JxtAccount(){}
}
