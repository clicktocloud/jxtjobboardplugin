package com.ctc.jobboard.jxt.service;

import java.io.IOException;

import javax.xml.bind.JAXBException;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.ctc.jobboard.defaultlist.Area;
import com.ctc.jobboard.defaultlist.Classification;
import com.ctc.jobboard.defaultlist.Country;
import com.ctc.jobboard.defaultlist.Location;
import com.ctc.jobboard.defaultlist.SubClassification;
import com.ctc.jobboard.jxt.utils.JXTConfig;
import com.ctc.jobboard.marshaller.MarshallerHelper;

public class JXTGetDefaultListServiceTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() throws IOException, JAXBException {
		JXTGetDefaultListService defaultService = new JXTGetDefaultListService("JXT","JXT__c");
		
		
		//defaultService.retrieveDefaultList("00D90000000gtFz");
		defaultService.retrieveDefaultList(JXTConfig.JXT_TEST_ADVERTISER_ID, JXTConfig.JXT_TEST_USERNAME, JXTConfig.JXT_TEST_PASSWORD,"");
		
		System.out.println(MarshallerHelper.convertObjecttoJson(defaultService.getCountryList()));
		System.out.println("Countries");
		for(Country c : defaultService.getCountryList().getCountries()){
			
			System.out.println(c.getCountryId() + " - " + c.getCountryName());
			
		}
		
		System.out.println("Locations");
		for(Location l : defaultService.getLocationList().getLocation()){
			
			System.out.println(l.getLocationId() + " - " + l.getLocationName() + " - " + l.getCountryId());
			
		}
		
		
		System.out.println("Areas");
		for(Area a : defaultService.getAreaList().getArea()){
			
			System.out.println(a.getAreaId() + " - " + a.getAreaName() + " - " + a.getLocationId() + " - " + a.getCountryId());
			
		}
		
		System.out.println("Classification");
		for(Classification c  : defaultService.getClassificationList().getClassification()){
			
			System.out.println(c.getId()+ " - " + c.getName() );
			
		}
		
		System.out.println("SubClassification");
		for(SubClassification c  : defaultService.getSubClassificationList().getSubClassification()){
			
			System.out.println(c.getId()+ " - " + c.getName() + " - " + c.getParentId() );
			
		}	
	}

}
