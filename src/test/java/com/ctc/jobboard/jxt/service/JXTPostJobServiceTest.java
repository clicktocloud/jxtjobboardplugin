package com.ctc.jobboard.jxt.service;

import java.io.IOException;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import com.ctc.jobboard.core.JobBoard;
import com.ctc.jobboard.jxt.domain.ext.JobPostResponse;
import com.ctc.jobboard.jxt.utils.JXTConfig;
import com.ctc.jobboard.util.JobBoardHelper;

public class JXTPostJobServiceTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Ignore
	@Test
	public void test() throws IOException {
		JXTPostJobService postService = new JXTPostJobService("JXT","JXT__c");
		
		String jobXmlContent = JobBoardHelper.readFileToString("/JobListingSample2.xml");
		
		JobPostResponse response = postService.postJob("00D90000000gtFz", "00D90000000gtFz:a0090000005cDR5AAM",JobBoard.SF_POSTING_STATUS_INQUEUE, jobXmlContent);
		
		System.out.println("Posted : " + (response.getSummary().getInserted() + response.getSummary().getUpdated()));
		
	}
	
	
	@Test
	public void testJsonJob() throws IOException {
		JXTPostJobService postService = new JXTPostJobService("JXT","JXT__c");
		
		String jobJsonContent = JobBoardHelper.readFileToString("/JobListingSample.json");
		
		//JobPostResponse response = postService.postJsonJob("00D90000000gtFz", "00D90000000gtFz:a0090000005cDR5AAM", jobJsonContent);
		JobPostResponse response = postService.postJsonJob("00D90000000gtFz", "00D90000000gtFz:a0090000005cDR5AAM",JobBoard.SF_POSTING_STATUS_INQUEUE, jobJsonContent,JXTConfig.JXT_TEST_ADVERTISER_ID,JXTConfig.JXT_TEST_USERNAME,JXTConfig.JXT_TEST_PASSWORD,"");
		
		System.out.println("Posted : " + (response.getSummary().getInserted() + response.getSummary().getUpdated()));
		
	}
	

}
