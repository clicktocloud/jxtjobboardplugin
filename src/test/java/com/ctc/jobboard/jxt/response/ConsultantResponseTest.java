package com.ctc.jobboard.jxt.response;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import javax.xml.bind.JAXBException;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.ctc.jobboard.jxt.domain.APIReturn;
import com.ctc.jobboard.jxt.domain.ConsultantResponse;
import com.ctc.jobboard.marshaller.MarshallerHelper;
import com.ctc.jobboard.util.JobBoardHelper;

public class ConsultantResponseTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() throws IOException, JAXBException {
		String xml = JobBoardHelper.readFileToString("/ConsultantResponseSample.xml");
		ConsultantResponse r = MarshallerHelper.convertXMLtoObject(xml, ConsultantResponse.class);
		
		assertEquals(APIReturn.SUCCESS,r.getResponseCode());
		
		assertEquals(122,r.getConsultants().getConsultantList().get(0).getConsultantID().intValue());
	}

}
