package com.ctc.jobboard.jxt.component;

import org.apache.log4j.Logger;

import com.ctc.jobboard.exception.AccountException;
import com.ctc.jobboard.jxt.domain.DefaultRequest;
import com.ctc.jobboard.jxt.domain.DefaultResponse;

public abstract class JXTDefaultListBoards extends JXTJobBoard{
	
	static Logger logger = Logger.getLogger("com.ctc.jobboard");
	
	
	protected DefaultResponse response;  
	
	protected JXTDefaultListBoards() {
		super( );
	}
	
	
	protected JXTDefaultListBoards(String jobBoardName) {
		super(jobBoardName);
	}
	
	/**
	 * Constructor 
	 * 
	 * @param jobBoardName
	 * @param jobBoardSfApiName
	 */
	protected JXTDefaultListBoards(String jobBoardName, String jobBoardSfApiName) {
		super(jobBoardName, jobBoardSfApiName);
	}
	
	
	@Override
	public void post2JobBoard() {
		
		DefaultRequest request = new DefaultRequest();
		
		try {
			
			setCredential(request);

			DefaultResponse response = execute(request,DefaultResponse.class);
			
			
			setResponse( response );
			
		} catch (AccountException e) {
			logger.error(e);
		} catch( Exception e){
			logger.error("Unknown error ", e);
		}
	}
	
	
	
	@Override
	public void responseHandler(){
		
		if( getResponse() != null ){
			
			logger.debug("Code : " +getResponse().getResponseCode());
			logger.debug("Message : " +getResponse().getResponseMessage());
			
		}
		
	}
	
	
	
	
	public DefaultResponse getResponse() {
		return response;
	}

	public void setResponse(DefaultResponse response) {
		this.response = response;
	}
	
	

	

	

}
