package com.ctc.jobboard.jxt.service;



import org.apache.log4j.Logger;

import com.ctc.jobboard.defaultlist.AdvertiserJobTemplateLogoList;
import com.ctc.jobboard.defaultlist.Area;
import com.ctc.jobboard.defaultlist.AreaList;
import com.ctc.jobboard.defaultlist.Classification;
import com.ctc.jobboard.defaultlist.ClassificationList;
import com.ctc.jobboard.defaultlist.Country;
import com.ctc.jobboard.defaultlist.CountryList;
import com.ctc.jobboard.defaultlist.JobTemplateList;
import com.ctc.jobboard.defaultlist.Location;
import com.ctc.jobboard.defaultlist.LocationList;
import com.ctc.jobboard.defaultlist.SubClassification;
import com.ctc.jobboard.defaultlist.SubClassificationList;
import com.ctc.jobboard.defaultlist.WorkTypeList;
import com.ctc.jobboard.exception.JobBoardException;
import com.ctc.jobboard.jxt.component.JXTDefaultListBoards;
import com.ctc.jobboard.jxt.domain.ArrayOfAdvertiserJobTemplateLogo;
import com.ctc.jobboard.jxt.domain.ArrayOfCountryLocationArea;
import com.ctc.jobboard.jxt.domain.ArrayOfJobTemplate;
import com.ctc.jobboard.jxt.domain.ArrayOfProfessionRole;
import com.ctc.jobboard.jxt.domain.ArrayOfWorkType;
import com.ctc.jobboard.jxt.domain.CountryLocationArea;
import com.ctc.jobboard.jxt.domain.ProfessionRole;
import com.ctc.jobboard.persistence.JBConnectionException;
import com.ctc.jobboard.persistence.JBConnectionManager;
import com.ctc.jobboard.util.SfOrg;

public class JXTGetDefaultListService extends JXTDefaultListBoards{
	
	static Logger logger = Logger.getLogger("com.ctc.jobboard");
	
	

	
	public JXTGetDefaultListService(String jobBoardName,String jobBoardApiName) {
		super(jobBoardName, jobBoardApiName);
		
		
	}
	
	public JXTGetDefaultListService( ) {
		super( );
		
		
	}
	
	public JXTGetDefaultListService(String jobBoardName) {
		super(jobBoardName);
		
		
	}

	public JXTGetDefaultListService retrieveDefaultList(String orgId , String namespace) throws JobBoardException{
		if(orgId == null ){
			logger.error("Org Id can not be empty !");
			throw new JobBoardException("Org Id can not be empty !");
		}
		
		try {
			setNamespace(namespace);
			currentSforg = new SfOrg(orgId);
			currentConnection = JBConnectionManager.connect( orgId);

			makeJobFeeds();
			
		} catch (JBConnectionException e) {
			logger.error("Failed to connect to salesforce instance of org ["+ orgId +"]", e);
			throw new JobBoardException("Failed to connect to salesforce instance of org ["+ orgId +"]", e);
		}
		
		return this;
	
	}
	

	public JXTGetDefaultListService retrieveDefaultList( String advertiserId, String username, String password , String namespace) throws JobBoardException{
		
		setNamespace(namespace);
		
		setAccount( buildAccount(advertiserId, username, password) );
		
		makeJobFeeds();
		
		return this;
		
	
	}
	
	
	public JobTemplateList getJobTemplateList(){
		JobTemplateList list = new JobTemplateList();
		if( getResponse() != null && getResponse().getDefaultList() != null){
			ArrayOfJobTemplate jt = getResponse().getDefaultList().getJobTemplates();
			if(jt != null && jt.getJobTemplate() != null){
				list.getJobTemplate().addAll(jt.getJobTemplate());
			}
			
		}
		
		return list;
	}
	
	public WorkTypeList getWorkTypeList(){
		WorkTypeList list = new WorkTypeList();
		if( getResponse() != null && getResponse().getDefaultList() != null){
			ArrayOfWorkType wt = getResponse().getDefaultList().getWorkTypes();
			if( wt != null && wt.getWorkType() != null){
				list.getWorkType().addAll( wt.getWorkType() );
			}
		}
		
		return list;
	}
	
	public AdvertiserJobTemplateLogoList getAdvertiserJobTemplateLogoList(){
		AdvertiserJobTemplateLogoList list = new AdvertiserJobTemplateLogoList();
		if( getResponse() != null && getResponse().getDefaultList() != null){
			ArrayOfAdvertiserJobTemplateLogo tl = getResponse().getDefaultList().getAdvertiserJobTemplateLogos();
			if( tl != null && tl.getAdvertiserJobTemplateLogo()!= null){
				list.getAdvertiserJobTemplateLogo().addAll( tl.getAdvertiserJobTemplateLogo() );
			}
		}
		
		return list;
	}
	
	public CountryList getCountryList(){
		CountryList countryList = new CountryList();
		if( getResponse() != null && getResponse().getDefaultList() != null){
			ArrayOfCountryLocationArea clas = getResponse().getDefaultList().getCountryLocationAreas();
			if(clas != null && clas.getCountryLocationArea() != null){
				for( CountryLocationArea cla : clas.getCountryLocationArea()){
					
					Country c = new Country(cla.getCountryID(), cla.getCountryName());
					c.setLocationList( getLocationList( c.getCountryId() ));
					countryList.getCountries().add(c );
					
				}
			}
		}
		
		return countryList;
	}
	
	public LocationList getLocationList(){
		LocationList locationList = new LocationList();
		if( getResponse() != null && getResponse().getDefaultList() != null){
			ArrayOfCountryLocationArea clas = getResponse().getDefaultList().getCountryLocationAreas();
			if(clas != null && clas.getCountryLocationArea() != null){
				for( CountryLocationArea cla : clas.getCountryLocationArea()){
					
					Location l = new Location(cla.getLocationID(), cla.getLocationName(),cla.getCountryID());
					l.setAreaList( getAreaList( l.getLocationId() ));
					locationList.getLocation().add( l );
					
				}
			}
		}
		
		return locationList;
	}
	
	public LocationList getLocationList(String countryId){
		LocationList locationList = new LocationList();
		if(countryId == null)
			return locationList;
		
		if( getResponse() != null && getResponse().getDefaultList() != null){
			ArrayOfCountryLocationArea clas = getResponse().getDefaultList().getCountryLocationAreas();
			if(clas != null && clas.getCountryLocationArea() != null){
				for( CountryLocationArea cla : clas.getCountryLocationArea()){
					if(countryId.equals(cla.getCountryID())){
						
						Location l = new Location(cla.getLocationID(), cla.getLocationName(),cla.getCountryID());
						l.setAreaList( getAreaList( l.getLocationId() ));
						
						locationList.getLocation().add(l);
					}	
				}
			}
		}
		
		return locationList;
	}
	
	
	public AreaList getAreaList(){
		AreaList areaList = new AreaList();
		if( getResponse() != null && getResponse().getDefaultList() != null){
			ArrayOfCountryLocationArea clas = getResponse().getDefaultList().getCountryLocationAreas();
			if(clas != null && clas.getCountryLocationArea() != null){
				for( CountryLocationArea cla : clas.getCountryLocationArea()){
					areaList.getArea().add(new Area(cla.getAreaID(), cla.getAreaName(),cla.getLocationID(), cla.getCountryID()));
					
				}
			}
		}
		
		return areaList;
	}
	
	public AreaList getAreaList(String locationId){
		
		AreaList areaList = new AreaList();
		if(locationId == null)
			return areaList;
		
		if( getResponse() != null && getResponse().getDefaultList() != null){
			ArrayOfCountryLocationArea clas = getResponse().getDefaultList().getCountryLocationAreas();
			if(clas != null && clas.getCountryLocationArea() != null){
				for( CountryLocationArea cla : clas.getCountryLocationArea()){
					if(locationId.equals(cla.getLocationID())){
						areaList.getArea().add(new Area(cla.getAreaID(), cla.getAreaName(),cla.getLocationID(), cla.getCountryID()));
					}
					
					
				}
			}
		}
		
		return areaList;
	}
	
	public ClassificationList getClassificationList(){
		ClassificationList list = new ClassificationList();
		if( getResponse() != null && getResponse().getDefaultList() != null){
			ArrayOfProfessionRole prs = getResponse().getDefaultList().getProfessionRoles();
			if(prs != null && prs.getProfessionRole() != null){
				for( ProfessionRole pr : prs.getProfessionRole()){
					
					Classification c = new Classification(pr.getProfessionID(), pr.getProfessionName());
					c.setSubClassificationList( getSubClassificationList( c.getId() ) ) ;
					
					list.getClassification().add(c);
					
				}
			}
		}
		
		return list;
	}
	
	public SubClassificationList getSubClassificationList(){
		SubClassificationList list = new SubClassificationList();
		if( getResponse() != null && getResponse().getDefaultList() != null){
			ArrayOfProfessionRole prs = getResponse().getDefaultList().getProfessionRoles();
			if(prs != null && prs.getProfessionRole() != null){
				for( ProfessionRole pr : prs.getProfessionRole()){
					list.getSubClassification().add(new SubClassification(pr.getRoleID(), pr.getRoleName(), pr.getProfessionID()));
					
				}
			}
		}
		
		return list;
	}
	
	public SubClassificationList getSubClassificationList(String parentId){
		SubClassificationList list = new SubClassificationList();
		if(parentId == null)
			return list;
		
		if( getResponse() != null && getResponse().getDefaultList() != null){
			ArrayOfProfessionRole prs = getResponse().getDefaultList().getProfessionRoles();
			if(prs != null && prs.getProfessionRole() != null){
				for( ProfessionRole pr : prs.getProfessionRole()){
					if(parentId.equals(pr.getProfessionID())){
						list.getSubClassification().add(new SubClassification(pr.getRoleID(), pr.getRoleName(), pr.getProfessionID()));
					}
				}
			}
		}
		
		return list;
	}



	@Override
	public void makeJobFeeds() throws JobBoardException{
		try {
			
			
			
			logger.debug("Begin to get default list from job board["+jobBoardName+"]");
	
			post2JobBoard();
			
			logger.debug("-----------------------------------------");
			
			responseHandler();
			
		} catch (Exception e) {
			logger.error("Failed to get default list  "+e);
			
			throw new JobBoardException("Failed to get default list ",e);
		}
	}
	
	

}
