package com.ctc.jobboard.jxt.component;

import org.apache.log4j.Logger;

import com.ctc.jobboard.exception.AccountException;
import com.ctc.jobboard.exception.JobBoardException;
import com.ctc.jobboard.jxt.domain.ArrayOfJobListing;
import com.ctc.jobboard.jxt.domain.JobPostRequest;
import com.ctc.jobboard.jxt.domain.ext.JobPostResponse;
import com.ctc.jobboard.jxt.utils.JXTConfig;

public abstract class JXTJobPostBoards extends JXTJobBoard{
	
	static Logger logger = Logger.getLogger("com.ctc.jobboard");
	
	
	
	protected JobPostResponse response;  
	protected ArrayOfJobListing jobListings = new ArrayOfJobListing();
	

	protected JXTJobPostBoards(  ) {
		super( );
	}
	
	protected JXTJobPostBoards(String jobBoardName) {
		super(jobBoardName);
	}
	
	
	/**
	 * Constructor 
	 * 
	 * @param jobBoardName
	 * @param jobBoardSfApiName
	 */
	protected JXTJobPostBoards(String jobBoardName, String jobBoardSfApiName) {
		super(jobBoardName, jobBoardSfApiName);
	}
	
	
	@Override
	public void post2JobBoard() {
		
		JobPostRequest request = new JobPostRequest();
		
		
		try {
			
			setCredential(request);
			
			request.setArchiveMissingJobs( JXTConfig.archiveMissingJobs );
			
			//
			ArrayOfJobListing listings = getJobListings();
			if( listings != null && listings.getJobListing().size() > 0){
				request.setListings(listings);
				

				JobPostResponse response = execute(request, JobPostResponse.class);
				
				setResponse( response );
				
			}else{
				logger.info("No any job to be posted from org org ["+getCurrentSforg().getOrgId()+"]");
			}
		} catch (AccountException e) {
			
			throw new JobBoardException("JxtAccountException - " + e.getMessage());
		} catch( Exception e){
			
			throw new JobBoardException("Other error - " + e.getMessage());
		}
	}
	
	
	
	
	
	
//	protected void throwJxtExceptionFromUnsuccessfulJobPostResponse(){
//		if(getResponse() != null ){
//			if( getResponse().getResponseCode() != APIReturn.SUCCESS){
//				throw new JobBoardException("JXT Response - " + getResponse().getResponseCode()+" :" + getResponse().getResponseMessage());
//			}
//			
//		}	
//	}
	
	public JobPostResponse getResponse() {
		return (JobPostResponse) super.getResponse();
	}

	public void setResponse(JobPostResponse response) {
		super.setResponse(response);
		super.setResponsePackage(new JXTResponsePackage(response,successCodes));
	}
	
	public ArrayOfJobListing getJobListings() {
		return jobListings;
	}



}
