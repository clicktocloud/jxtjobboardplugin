package com.ctc.jobboard.jxt.service;



import java.util.List;

import org.apache.log4j.Logger;

import com.ctc.jobboard.core.JobBoard;
import com.ctc.jobboard.exception.JobBoardException;
import com.ctc.jobboard.jxt.component.JXTJobArchiveBoards;
import com.ctc.jobboard.jxt.domain.APIReturn;
import com.ctc.jobboard.jxt.domain.ext.Job;
import com.ctc.jobboard.jxt.domain.ext.JobPostResponse;
import com.ctc.jobboard.persistence.JBConnectionManager;
import com.ctc.jobboard.util.SfOrg;

public class JXTArchiveJobService extends JXTJobArchiveBoards{
	
	static Logger logger = Logger.getLogger("com.ctc.jobboard");

	protected List<String> referenceNos;
	
	public JXTArchiveJobService( ) {
		super( );
		
	}
	
	public JXTArchiveJobService(String jobBoardName) {
		super(jobBoardName);
		
	}
	
	public JXTArchiveJobService(String jobBoardName, String jobBoardSfApiName) {
		super(jobBoardName, jobBoardSfApiName);
		
	}
	
	public JobPostResponse archiveJob(String orgId,  List<String> referenceNos, String advertiserId, String username, String password, String namespace) throws JobBoardException{ 
		
		setAccount( buildAccount(advertiserId, username, password));
		setNamespace(namespace);
		
		return archiveJob(orgId, referenceNos);
	}

	public JobPostResponse archiveJob(String orgId,  List<String> referenceNos) throws JobBoardException{
		if(orgId == null || referenceNos==null || referenceNos.size() == 0){
			logger.error("Org Id and reference No can not be empty !");
			throw new JobBoardException("Org Id, reference No can not be empty !");
		}

		currentSforg = new SfOrg(orgId);
		this.referenceNos =  referenceNos;
		if(this.referenceNos != null && this.referenceNos.size() == 1){
			this.referenceNo = this.referenceNos.get(0);
		}

		try {
			
			makeJobFeeds();
			
		} catch (JobBoardException e) {
			logger.error("JxtException - ", e);
			
			JobPostResponse response = createJobPostResponse(APIReturn.ERROR,e.getMessage(),JobBoard.ERROR, e.getMessage(), this.referenceNos);
			
			setResponse(response);
		}
		
		logger.debug("Begin to handler reponse");
		
		responseHandler();
		
		logger.debug("-----------------------------------------");
		
		return getResponse();
	
	}

	@Override
	public void makeJobFeeds() throws JobBoardException{
		try {
			
			logger.debug("Begin to make the list of archive job ads for org ["+currentSforg.getOrgId()+"]");
			
			for(String referNo : referenceNos){
				Job job = new Job();
				
				job.setReferenceNo(referNo);
				archiveJobListings.getJob().add(job);
				
			}

			logger.debug("-----------------------------------------");
			
			logger.debug("Connecting to Salesforce instance of org ["+ currentSforg.getOrgId() +"]");
			
			currentConnection = JBConnectionManager.connect( currentSforg.getOrgId() );
			
			logger.debug("-----------------------------------------");
			
			//
			logger.debug("Begin to archive " + archiveJobListings.getJob().size() + " job ads from job board["+jobBoardName+"]");
			
			post2JobBoard();
			
			logger.debug("-----------------------------------------");
			
			
			
			
		}catch (JobBoardException e) {
			logger.error(e);
			throw e;
			
		}  catch (Exception e) {
			logger.error("Failed to archive job ads of orgId="+currentSforg.getOrgId()+"  "+e);
			throw new JobBoardException("Other error - ",e);
		}
	}

}
