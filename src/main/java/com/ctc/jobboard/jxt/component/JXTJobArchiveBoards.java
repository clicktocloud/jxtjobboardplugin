package com.ctc.jobboard.jxt.component;

import org.apache.log4j.Logger;

import com.ctc.jobboard.core.JobBoard;
import com.ctc.jobboard.exception.AccountException;
import com.ctc.jobboard.exception.JobBoardException;
import com.ctc.jobboard.jxt.domain.ArchiveJobRequest;
import com.ctc.jobboard.jxt.domain.ext.ArrayOfJob;
import com.ctc.jobboard.jxt.domain.ext.JobPostResponse;

public abstract class JXTJobArchiveBoards extends JXTJobPostBoards{
	
	static Logger logger = Logger.getLogger("com.ctc.jobboard");
	
	
	protected ArrayOfJob archiveJobListings = new ArrayOfJob();
	
	
	protected JXTJobArchiveBoards() {
		super( );
	}
	
	
	protected JXTJobArchiveBoards(String jobBoardName) {
		super(jobBoardName);
	}
	
	{
		this.actionType = JobBoard.ARCHIVE;
	}
	
	
	/**
	 * Constructor 
	 * 
	 * @param jobBoardName
	 * @param jobBoardSfApiName
	 */
	protected JXTJobArchiveBoards(String jobBoardName, String jobBoardSfApiName) {
		super(jobBoardName, jobBoardSfApiName);
	}
	
	
	@Override
	public void post2JobBoard() {
		
		ArchiveJobRequest request = new ArchiveJobRequest();
	
		try {
			
			setCredential(request);

			//
			ArrayOfJob listings = getArchiveJobListings();
			if( listings != null && listings.getJob().size() > 0){
				request.setListings(listings);
				
				JobPostResponse response = execute(request, JobPostResponse.class);
				
				setResponse( response );
			}else{
				logger.info("No any job to be posted from org org ["+getCurrentSforg().getOrgId()+"]");
			}
		} catch (AccountException e) {
			
			throw new JobBoardException("JxtAccountException - " + e.getMessage());
		} catch( Exception e){
			
			throw new JobBoardException("Other error - " + e.getMessage());
		}
	}
	
	
	
	public ArrayOfJob getArchiveJobListings() {
		return archiveJobListings;
	}

//	protected void addFailureJBObjects(String aid, String failureMessage){
//		super.addArchiveFailureJBObjects(aid, failureMessage);
//	}
	

}
