package com.ctc.jobboard.jxt.service;

import java.util.Arrays;

import com.ctc.jobboard.core.JobBoardCredential;
import com.ctc.jobboard.core.JobBoardResponsePackage;
import com.ctc.jobboard.defaultlist.DefaultList;
import com.ctc.jobboard.exception.JobBoardException;
import com.ctc.jobboard.plugin.JobBoardPluginInterface;

public class JXTJobBoardPlugin implements JobBoardPluginInterface {
	
	
	
	public JXTJobBoardPlugin() {
		super();
		
	}
	
	public JXTJobBoardPlugin(String boardName) {
		super();
		this.boardName = boardName;
		
	}


	private String boardName;
	
	@Override
	public void setJobBoardName(String boardName) {
		this.boardName = boardName;
		
	}

	

	@Override
	public JobBoardResponsePackage createJob(String referenceNo, String postingStatus,
			String jobJsonContent, JobBoardCredential credential) throws JobBoardException{
		JXTPostJobService service = new JXTPostJobService(boardName);
		service.postJsonJob(credential.getOrgId(), referenceNo, postingStatus, jobJsonContent, credential.getAdvertiserId(), credential.getClientId(), credential.getClientSecret(), credential.getNamespace());
		
		return service.getResponsePackage();
	}

	@Override
	public JobBoardResponsePackage updateJob(String referenceNo, String jobBoardAdId,
			String postingStatus, String jobJsonContent,
			JobBoardCredential credential) throws JobBoardException{
		
		JXTPostJobService service = new JXTPostJobService(boardName);
		service.postJsonJob(credential.getOrgId(), referenceNo,postingStatus, jobJsonContent, credential.getAdvertiserId(), credential.getClientId(), credential.getClientSecret(), credential.getNamespace());
		
		return service.getResponsePackage();
	}

	@Override
	public JobBoardResponsePackage archiveJob(String referenceNo, String jobBoardAdId,
			String postingStatus, JobBoardCredential credential) throws JobBoardException{
		
		JXTArchiveJobService service = new JXTArchiveJobService(boardName);
		service.archiveJob(credential.getOrgId(), Arrays.asList(new String[]{referenceNo}),  credential.getAdvertiserId(), credential.getClientId(), credential.getClientSecret(), credential.getNamespace());
		
		return service.getResponsePackage();
	}

	@Override
	public JobBoardResponsePackage getAllJobs(JobBoardCredential credential) throws JobBoardException{
		//TODO
		return null;
	}

	@Override
	public JobBoardResponsePackage getJob(String jobBoardAdId,
			JobBoardCredential credential) throws JobBoardException {
		//TODO
		return null;
	}


	@Override
	public DefaultList getDefaultList(JobBoardCredential credentials) {
		DefaultList defaultList = new DefaultList();
		JXTGetDefaultListService service = new JXTGetDefaultListService(boardName);
		
		service.retrieveDefaultList(credentials.getAdvertiserId(), credentials.getClientId(), credentials.getClientSecret(),credentials.getNamespace());
		
		defaultList.setClassificationList( service.getClassificationList() );
		defaultList.setCountryList(service.getCountryList());
		defaultList.setAdvertiserJobTemplateLogoList(service.getAdvertiserJobTemplateLogoList());
		defaultList.setJobTemplateList(service.getJobTemplateList());
		defaultList.setSalaryRange(null);
		defaultList.setSalaryTypeList( null);
		defaultList.setWorkTypeList(service.getWorkTypeList());
		return defaultList;
	}

	
	

}
